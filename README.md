# List Folder Users Script
Exports a list of users associated with the sub-folders for the folder specified to a Bootstrap HTML page

## Instructions
1. Open Powershell
2. ```$> ./list-folder-user.ps1 > group_permissions.html```
3. Enter the root folder path at the prompt. For instance, entering \\some\folder\path would list all of the folders under this path
```cmdlet test2.ps1 at command pipeline position 1
Supply values for the following parameters:
Path:
```
4. Open html file in web browser