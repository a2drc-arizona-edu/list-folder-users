[CmdletBinding()]
Param (
    [ValidateScript({Test-Path $_ -PathType Container})]
    [Parameter(Mandatory=$true)]
    [string]$Path
)

echo "<!DOCTYPE html>
<html>
<head>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
  <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
  <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>
</head>
<body>

<div class='container'>"

$securityFoldersWithIssues = @()
$Domain = (Get-ADDomain).NetBIOSName
$Folders = Get-ChildItem -Path $Path -Directory -Force -ErrorAction SilentlyContinue

ForEach ($Folder in $Folders) {
    $folderUsers = @()

    try {
        $ACLs = Get-Acl $Folder.FullName | ForEach-Object { $_.Access }
        echo "<a data-toggle='collapse' href='#$($Folder.Name)_content'><h4>$($Folder.FullName)</h4></a>"

        ForEach ($ACL in $ACLs) {   
            If ($ACL.IdentityReference -match "\\") {   
                If ($ACL.IdentityReference.Value.Split("\")[0].ToUpper() -eq $Domain.ToUpper()) {   
                    $Name = $ACL.IdentityReference.Value.Split("\")[1]

                    If ((Get-ADObject -Filter 'SamAccountName -eq $Name').ObjectClass -eq "group") {   
                        ForEach ($User in (Get-ADGroupMember $Name -Recursive | Select -ExpandProperty Name))
                        {   
                            $Result = New-Object PSObject -Property @{
                                Path = $Folder.Fullname
                                Group = $Name
                                User = $User
                                FileSystemRights = $ACL.FileSystemRights
                                AccessControlType = $ACL.AccessControlType
                                Inherited = $ACL.IsInherited
                            }
                            $folderUsers += $Result
                        }
                    } Else {    
                        $Result = New-Object PSObject -Property @{
                            Path = $Folder.Fullname
                            Group = ""
                            User = Get-ADUser $Name | Select -ExpandProperty Name
                            FileSystemRights = $ACL.FileSystemRights
                            AccessControlType = $ACL.AccessControlType
                            Inherited = $ACL.IsInherited
                        }
                        $folderUsers += $Result
                    }
                } Else {   
                    $Result = New-Object PSObject -Property @{
                        Path = $Folder.Fullname
                        Group = ""
                        User = $ACL.IdentityReference.Value
                        FileSystemRights = $ACL.FileSystemRights
                        AccessControlType = $ACL.AccessControlType
                        Inherited = $ACL.IsInherited
                    }
                    $folderUsers += $Result
                }
            }
        }

        echo "<div id='$($Folder.Name)_content' class='collapse multi-collapse'><pre>"
        echo $folderUsers | Sort-Object -Property Group | Format-Table -AutoSize -Property User, Group, FileSystemRights, AccessControlType
        echo "</pre></div>"
    } catch {
        $securityFoldersWithIssues += $Folder.Name
    }
}

echo "</div>"

if ($securityFoldersWithIssues.Count > 0) {
    echo "<h1 style='color: red'>Folders with Issues</h1>"
    echo "<ul>"
    ForEach ($fld in $securityFoldersWithIssues) {   
        echo "<li>$($fld)</li>"
    }
    echo "</ul>"
}
echo "</body></html>"